package com.me.services;

import com.me.model.Book;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Maximiliano on 9/25/15 AD.
 */
public class BookServicesTest {

    private BookServices bookServices;
    @Before
    public void setUp() throws Exception {
        bookServices = new BookServices();

    }

    @After
    public void tearDown() throws Exception {
        bookServices = null;

    }

    @org.junit.Test
    public void testGetAllBook() throws Exception {
        BookServices bookServices = new BookServices();
        Assert.assertNotNull(bookServices.getAllBook());
    }

    @Test
    public void testGetBookById() throws Exception {
        BookServices bookServices = new BookServices();
        Book book = bookServices.getBookById(2);
        Assert.assertEquals("Harry Potter II",book.getTitle());
    }

    @Test
    public void testSearchBook() throws Exception {
        List<Book> bookList = bookServices.searchBook("pu");
        for (Book book : bookList){
            System.out.println(book);
        }
    }
}