package com.me.services;

import com.me.model.Author;
import junit.framework.Assert;
import org.junit.Test;

/**
 * Created by Maximiliano on 10/5/2015 AD.
 */
public class AuthorServiceTest {

    @Test
    public void testGetAllAuthors() throws Exception {
        AuthorService authorService = new AuthorService();
        org.junit.Assert.assertNotNull(authorService.getAllAuthors());
    }

    @Test
    public void testGetAuthorById() throws Exception {
        AuthorService authorService = new AuthorService();
        org.junit.Assert.assertEquals(authorService.getAuthorById(1).getAuthorName(),"J.K. Rowling");
    }
}