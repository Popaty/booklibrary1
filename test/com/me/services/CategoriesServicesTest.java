package com.me.services;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Maximiliano on 9/28/15 AD.
 */
public class CategoriesServicesTest {
    private CategoriesServices categoriesServices;
    @Before
    public void setUp() throws Exception {
        categoriesServices = new CategoriesServices();

    }

    @After
    public void tearDown() throws Exception {
        categoriesServices = null;

    }

    @Test
    public void testGetCategoriesListByBookId() throws Exception {
        Assert.assertNotNull(categoriesServices.getCategoriesListByBookId(1));
    }

    @Test
    public void testGetAllCategories() throws Exception {
        Assert.assertNotNull(categoriesServices.getAllCategories());
    }

    @Test
    public void testGetIdCategoriesByName() throws Exception {
        String name = "นิยาย";
        Assert.assertNotNull(categoriesServices.getIdCategoriesByName(name));
        Assert.assertEquals(name,categoriesServices.getCategoriesById(categoriesServices.getIdCategoriesByName(name)).getName());

    }
}