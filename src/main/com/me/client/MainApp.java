package com.me.client;

import com.me.model.Book;
import com.me.services.BookServices;

import java.util.List;

/**
 * Created by Maximiliano on 9/25/15 AD.
 */
public class MainApp {
    public static void main(String[] args) {
        BookServices bookServices = new BookServices();
        List<Book> bookList = bookServices.getAllBook();
        for (Book currentBook : bookList){
            System.out.println(currentBook);
        }

    }
}
