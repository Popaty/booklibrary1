package com.me.mappers;

import com.me.model.Categories;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by Maximiliano on 9/28/15 AD.
 */
public interface CategoriesMapper {

    @Select("select * from categories order by id")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "name", column = "name")
    })
    public List<Categories> getAllCategories();

    @Select("select * from categories where id = #{id}")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "name", column = "name")
    })
    public Categories getCategoriesById(int id);

    @Select("SELECT * from categories c inner join join_book_categories jbc " +
            "on c.id = jbc.cat_id " +
            "where jbc.book_id = #{id}")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "name", column = "name")
    })
    public List<Categories> getCategoriesListByBookId(int id);

    @Select("select id from categories where name = #{name}")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "name", column = "name")
    })
    public int getIdCategoriesByName(String name);



}
