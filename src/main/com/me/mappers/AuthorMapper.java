package com.me.mappers;

import com.me.model.Author;
import com.me.model.Categories;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by Maximiliano on 10/5/2015 AD.
 */
public interface AuthorMapper {
    @Select("select * from author order by id")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "authorName", column = "name")
    })
    public List<Author> getAllAuthors();

    @Select("select * from author where id = #{id}")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "authorName", column = "name")
    })
    public Author getAuthorById(int id);


}
