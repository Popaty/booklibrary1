package com.me.mappers;

import com.me.model.Book;
import com.me.model.Categories;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by Maximiliano on 9/25/15 AD.
 */
public interface BookMapper {

    @Select("select * from book b order by id")
    @Results({
            @Result(id=true, property = "isbn",column = "isbn"),
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "author", column = "author"),
            @Result(property = "description", column = "description"),
            @Result(property = "publisher", column = "publisher_id",
            one = @One(select = "com.me.mappers.PublisherMapper.getPublisherById")),
            @Result(property = "categoriesList", column = "id",
            many = @Many(select = "com.me.mappers.CategoriesMapper.getCategoriesListByBookId")),
            @Result(property = "coverPic", column = "cover_pic")
    })
    List<Book> getAllBook();

    @Select("select * from book where id = #{id}")
    @Results({
            @Result(id=true, property = "isbn",column = "isbn"),
            @Result(property = "title", column = "title"),
            @Result(property = "author", column = "author"),
            @Result(property = "description", column = "description"),
    })
    public Book getBookById(int id);


    @Insert("insert into book (isbn,title,author,description,publisher_id,cover_pic) " +
            "values (#{isbn},#{title},#{author},#{description},#{publisher.id},#{coverPic})")
    public void insertBook(Book book);

    @Insert("insert into join_book_categories (book_id,cat_id) " +
            "VALUES (#{param1},#{param2})")
    public void addCategories(int book_id, int cat_id);

    @Update("UPDATE BOOK SET " +
            "FIRST_NAME = #{firstName}, " +
            "DOB = #{dob}" +
            "WHERE ID = #{id}")
    public void updateBook(Book book);

    @Delete("DELETE FROM book " +
            "WHERE ID = #{id}")
    public void deleteBook(Long id);


    @Select("SELECT *\n" +
            "FROM book\n" +
            "WHERE ID = (SELECT MAX(ID) AS id FROM book)")
    @Results({
            @Result(id=true, property = "isbn",column = "isbn"),
            @Result(property = "title", column = "title"),
            @Result(property = "author", column = "author"),
            @Result(property = "description", column = "description")
    })
    public Book getBookByMaxId();

    @Select("select * from book " +
            "where title " +
            "like CONCAT('%', #{param1}, '%')")
    @Results({
            @Result(id=true, property = "isbn",column = "isbn"),
            @Result(property = "title", column = "title"),
            @Result(property = "author", column = "author"),
            @Result(property = "description", column = "description")
    })
    public List<Book> searchBook(String searchtext);

}
