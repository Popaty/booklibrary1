package com.me.mappers;

import com.me.model.Publisher;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by Maximiliano on 9/25/15 AD.
 */
public interface PublisherMapper {

    @Select("select * from publisher")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "name", column = "name_publisher"),
            @Result(property = "address", column = "address")
    })
    public List<Publisher> getAllPublisher();

    @Select("select * from publisher where id = #{id}")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "name", column = "name_publisher"),
            @Result(property = "address", column = "address")
    })
    public Publisher getPublisherById(int id);
}
