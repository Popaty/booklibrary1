package com.me.model;

/**
 * Created by Maximiliano on 9/28/15 AD.
 */
public class Categories {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name+" ";
    }
}
