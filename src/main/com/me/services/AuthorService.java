package com.me.services;

import com.me.mappers.AuthorMapper;
import com.me.model.Author;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * Created by Maximiliano on 10/5/2015 AD.
 */
public class AuthorService implements AuthorMapper {
    @Override
    public List<Author> getAllAuthors() {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            AuthorMapper authorMapper = sqlSession.getMapper(AuthorMapper.class);
            return authorMapper.getAllAuthors();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Author getAuthorById(int id) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            AuthorMapper authorMapper = sqlSession.getMapper(AuthorMapper.class);
            return authorMapper.getAuthorById(id);
        } finally {
            sqlSession.close();
        }
    }
}
