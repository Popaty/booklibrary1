package com.me.services;

import com.me.mappers.CategoriesMapper;
import com.me.model.Categories;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * Created by Maximiliano on 9/28/15 AD.
 */
public class CategoriesServices implements CategoriesMapper {

    private List<String> selectedListCategories;
    private int selectedCategories;

    public List<String> getSelectedListCategories() {
        return selectedListCategories;
    }

    public void setSelectedListCategories(List<String> selectedListCategories) {
        this.selectedListCategories = selectedListCategories;
    }

    public int getSelectedCategories() {
        return selectedCategories;
    }

    public void setSelectedCategories(int selectedCategories) {
        this.selectedCategories = selectedCategories;
    }

    @Override
    public List<Categories> getAllCategories() {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            CategoriesMapper categoriesMapper = sqlSession.getMapper(CategoriesMapper.class);
            return categoriesMapper.getAllCategories();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Categories getCategoriesById(int id) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            CategoriesMapper categoriesMapper = sqlSession.getMapper(CategoriesMapper.class);
            return categoriesMapper.getCategoriesById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<Categories> getCategoriesListByBookId(int id) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            CategoriesMapper catogoriesMapper = sqlSession.getMapper(CategoriesMapper.class);
            return catogoriesMapper.getCategoriesListByBookId(id);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int getIdCategoriesByName(String name) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            CategoriesMapper categoriesMapper = sqlSession.getMapper(CategoriesMapper.class);
            return categoriesMapper.getIdCategoriesByName(name);
        } finally {
            sqlSession.close();
        }
    }
}
