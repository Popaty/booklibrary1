package com.me.services;

import com.me.mappers.BookMapper;
import com.me.model.Book;
import com.me.model.Categories;
import com.me.model.Publisher;
import org.apache.ibatis.session.SqlSession;
import org.primefaces.model.UploadedFile;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.server.UID;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Maximiliano on 9/25/15 AD.
 */
@ManagedBean(name = "book")
@SessionScoped
public class BookServices implements BookMapper {

    private Book selectedBook;
    private Book bookEntity;

    public Book getBookEntity() {
        return bookEntity;
    }

    public void setBookEntity(Book bookEntity) {
        this.bookEntity = bookEntity;
    }

    public Book getSelectedBook() {return selectedBook;}

    public void setSelectedBook(Book selectedBook) {
        this.selectedBook = selectedBook;
    }

    public String returnHome(){
        return "/home.xhtml?faces-redirect=true";
    }

//    public String returnIndex(){
//        return "/index.xhtml?faces-redirect=true";
//    }

    public String startCreateBook(){
        FacesContext facesContext = FacesContext.getCurrentInstance();
        CategoriesServices categoriesServicesBean = (CategoriesServices) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext,"categories");
        PublisherServices publisherServicesBean = (PublisherServices) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext,"publisher");
        ImageServices imageServicesBean = (ImageServices) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext,"image");
        bookEntity = new Book();
        categoriesServicesBean.setSelectedListCategories(new ArrayList<>());
        publisherServicesBean.setSelectedPublisher(0);
        imageServicesBean.setUploadedFile(new UploadedFile() {
            @Override
            public String getFileName() {
                return null;
            }

            @Override
            public InputStream getInputstream() throws IOException {
                return null;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public byte[] getContents() {
                return new byte[0];
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public void write(String filePath) throws Exception {

            }
        });


        return "/addPage.xhtml?faces-redirect=true";
    }

    public String CreateBook(Book book){
//        System.out.println(book);
        return "/index.xhtml?faces-redirect=true";
    }

    public String CreateBook(){
//        System.out.println(bookEntity);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        CategoriesServices categoriesServicesBean = (CategoriesServices) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext,"categories");
        PublisherServices publisherServicesBean = (PublisherServices) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext,"publisher");
        ImageServices imageServicesBean = (ImageServices) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext,"image");

        int publisher = publisherServicesBean.getSelectedPublisher();
        int categories = categoriesServicesBean.getSelectedCategories();
        String coverBook = imageServicesBean.getUploadedFile().getFileName();
        bookEntity.setIsbn("ISBN" + Math.random());
        bookEntity.setPublisher(publisherServicesBean.getPublisherById(publisher));
        bookEntity.setCoverPic("test1.png");
        bookEntity.setCoverPic(coverBook);
        insertBook(bookEntity);

        Book book = getBookByMaxId();
//        System.out.println(categoriesServicesBean.getSelectedListCategories());
        List<String> listCategories = categoriesServicesBean.getSelectedListCategories();
        for (String stringCat : listCategories){
//            System.out.println(stringCat);
            int catListId = categoriesServicesBean.getIdCategoriesByName(stringCat.trim());
//            System.out.println(catListId);
            addCategories(book.getId(),catListId);
        }


//        addCategories(book.getId(),categories1.getId());

        return "/index.xhtml?faces-redirect=true";
    }

    @Override
    public List<Book> getAllBook() {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            return bookMapper.getAllBook();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Book getBookById(int id) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            return bookMapper.getBookById(id);
        } finally {
            sqlSession.close();
        }
    }


    @Override
    public void insertBook(Book book) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            bookMapper.insertBook(book);
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void addCategories(int book_id, int cat_id) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            bookMapper.addCategories(book_id,cat_id);
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
    }


    @Override
    public void updateBook(Book book) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            bookMapper.updateBook(book);
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void deleteBook(Long id) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            bookMapper.deleteBook(id);
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }

    }

    @Override
    public Book getBookByMaxId() {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            return bookMapper.getBookByMaxId();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<Book> searchBook(String searchtext) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            BookMapper bookMapper = sqlSession.getMapper(BookMapper.class);
            return bookMapper.searchBook(searchtext);
        } finally {
            sqlSession.close();
        }
    }

    public String testInput(){
        System.out.println("Inside method");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        CategoriesServices categoriesServicesBean = (CategoriesServices) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext,"categories");

        System.out.println(categoriesServicesBean.getSelectedListCategories());
        System.out.println(categoriesServicesBean.getSelectedListCategories().getClass());
        List<String> list = categoriesServicesBean.getSelectedListCategories();

        for (String string : list){
            System.out.println(string);
        }
        return "";
    }
    public String editBook(){
        System.out.println(selectedBook);
        if (selectedBook == null){
            return "/index.xhtml?faces-redirect=true";
        }else
            bookEntity = getBookById(selectedBook.getId());
        return "/editPage.xhtml?faces-redirect=true";
    }

}
