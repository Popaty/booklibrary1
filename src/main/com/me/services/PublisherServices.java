package com.me.services;

import com.me.mappers.PublisherMapper;
import com.me.model.Publisher;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * Created by Maximiliano on 9/28/15 AD.
 */
public class PublisherServices implements PublisherMapper {

    private int selectedPublisher;

    public int getSelectedPublisher() {
        return selectedPublisher;
    }

    public void setSelectedPublisher(int selectedPublisher) {
        this.selectedPublisher = selectedPublisher;
    }

    @Override
    public List<Publisher> getAllPublisher() {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            PublisherMapper publisherMapper = sqlSession.getMapper(PublisherMapper.class);
            return publisherMapper.getAllPublisher();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Publisher getPublisherById(int id) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            PublisherMapper publisherMapper = sqlSession.getMapper(PublisherMapper.class);
            return publisherMapper.getPublisherById(id);
        } finally {
            sqlSession.close();
        }
    }
}
