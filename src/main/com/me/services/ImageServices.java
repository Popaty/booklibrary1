package com.me.services;

import com.sun.faces.vendor.Tomcat6InjectionProvider;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

/**
 * Created by Maximiliano on 9/29/15 AD.
 */

public class ImageServices {

    private UploadedFile uploadedFile;

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }
    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

//    public void handleFileUpload() {
//        System.out.println(uploadedFile);
//        if(uploadedFile != null) {
//            byte[] imagebyte = uploadedFile.getContents();
//            String nameImage = uploadedFile.getFileName();
//            try {
//                BufferedImage img = ImageIO.read(new ByteArrayInputStream(imagebyte));
//                String path = System.getProperty("user.home");
//                File testFile = new File(path+"/ProjectCode/booklibrary1/web/images/"+nameImage);
//                ImageIO.write(img,"png",testFile);
////                System.out.println(path);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }else {
//            System.out.println("This uploaded file is Null");
//        }
//
//    }
    public void handleFileUpload(FileUploadEvent event){
        uploadedFile = event.getFile();
        if(uploadedFile != null) {
            byte[] imagebyte = uploadedFile.getContents();
            String nameImage = uploadedFile.getFileName();
            try {
                BufferedImage img = ImageIO.read(new ByteArrayInputStream(imagebyte));
                String path = System.getProperty("user.home");
                File testFile = new File(path+"/ProjectCode/booklibrary1/web/images/"+nameImage);
                ImageIO.write(img,"png",testFile);
//                System.out.println(path);
                FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
                FacesContext.getCurrentInstance().addMessage(null, message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            System.out.println("This uploaded file is Null");
        }
    }
}
